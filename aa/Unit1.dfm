object Capitais_principal: TCapitais_principal
  Left = 614
  Top = 165
  Width = 525
  Height = 282
  Caption = 'Trabalho Trimestral - Capitais'
  Color = clActiveCaption
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'DejaVu Sans Condensed'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 18
  object List_de_capitais: TListBox
    Left = 0
    Top = 8
    Width = 205
    Height = 213
    ItemHeight = 18
    Items.Strings = (
      'Nome:'
      ''
      'Pa'#237's:'
      ''
      'Numero de Habitantes:'
      ''
      'Expan'#231#227'o Territorial:')
    TabOrder = 0
  end
  object Ed_ExTerritorial: TEdit
    Left = 227
    Top = 121
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    Text = 'Extens'#227'o territorial'
    OnClick = Ed_ExTerritorialClick
  end
  object Bt_inserir: TButton
    Left = 226
    Top = 160
    Width = 71
    Height = 25
    Caption = 'Inserir'
    TabOrder = 2
    OnClick = Bt_inserirClick
  end
  object Bt_remover: TButton
    Left = 306
    Top = 160
    Width = 71
    Height = 25
    Caption = 'Remover'
    TabOrder = 3
    OnClick = Bt_removerClick
  end
  object Bt_at: TButton
    Left = 266
    Top = 197
    Width = 71
    Height = 28
    Caption = 'Atualizar'
    TabOrder = 4
    OnClick = Bt_atClick
  end
  object Bt_save: TButton
    Left = 402
    Top = 65
    Width = 71
    Height = 23
    Caption = 'Salvar'
    TabOrder = 5
    OnClick = Bt_saveClick
  end
  object Ed_nome: TEdit
    Left = 227
    Top = 8
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 6
    Text = 'Nome da capital'
    OnClick = Ed_nomeClick
  end
  object Ed_pais: TEdit
    Left = 227
    Top = 45
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 7
    Text = 'Pa'#237's'
    OnClick = Ed_paisClick
  end
  object Ed_numerodehabitantes: TEdit
    Left = 227
    Top = 83
    Width = 153
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clGray
    Font.Height = -15
    Font.Name = 'DejaVu Sans Condensed'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 8
    Text = 'Numero de habitantes'
    OnClick = Ed_numerodehabitantesClick
  end
end
