unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TCapitais_principal = class(TForm)
    List_de_capitais: TListBox;
    Ed_ExTerritorial: TEdit;
    Bt_inserir: TButton;
    Bt_remover: TButton;
    Bt_at: TButton;
    Bt_save: TButton;
    Ed_nome: TEdit;
    Ed_pais: TEdit;
    Ed_numerodehabitantes: TEdit;
    procedure Ed_ExTerritorialClick(Sender: TObject);
    procedure Ed_nomeClick(Sender: TObject);
    procedure Ed_paisClick(Sender: TObject);
    procedure Ed_numerodehabitantesClick(Sender: TObject);
    procedure Bt_inserirClick(Sender: TObject);
    procedure Bt_removerClick(Sender: TObject);
    procedure Bt_atClick(Sender: TObject);
    procedure Bt_saveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Capitais_principal: TCapitais_principal;

implementation
  type
    TCapitais = class(TObject)
    nome,pais,numerodehabitantes,ExTerritorial : string;
  end;

  var Capitais : TCapitais;
      CapitalI : TextFile;
{$R *.dfm}

  // Edits
procedure TCapitais_principal.Ed_nomeClick(Sender: TObject);
begin
  Ed_nome.Clear;
  Ed_nome.Font.Color := 4444;
end;

procedure TCapitais_principal.Ed_ExTerritorialClick(Sender: TObject);
begin
  Ed_ExTerritorial.Clear;
  Ed_ExTerritorial.Font.Color := 3333;
end;

procedure TCapitais_principal.Ed_paisClick(Sender: TObject);
begin
  Ed_pais.Clear;
  Ed_pais.Font.Color := 2222;
end;

procedure TCapitais_principal.Ed_numerodehabitantesClick(Sender: TObject);
begin
  Ed_numerodehabitantes.Clear;
  Ed_numerodehabitantes.Font.Color := 5555;
end;

procedure TCapitais_principal.Bt_inserirClick(Sender: TObject);
begin
Capitais := TCapitais.Create;

  If (Ed_nome.text <> '') and (Ed_pais.text <> '') and (Ed_numerodehabitantes.text <> '') and (Ed_ExTerritorial.text <> '') and
     (Ed_nome.text <> 'Nome da capital') and (Ed_pais.text <> 'Pa�s da capital') and (Ed_numerodehabitantes.text <> 'Numero de habitantes') and (Ed_ExTerritorial.text <> 'Tempo de vida')

   then
  begin
    Capitais.nome := Ed_nome.text;
    Capitais.pais := Ed_pais.text;
    Capitais.numerodehabitantes := Ed_numerodehabitantes.text;
    Capitais.ExTerritorial := Ed_ExTerritorial.text;

    Capitais_principal.List_de_Capitais.Items[1] := Capitais.nome;
    Capitais_principal.List_de_Capitais.Items[3] := Capitais.pais;
    Capitais_principal.List_de_Capitais.Items[5] := Capitais.numerodehabitantes;
    Capitais_principal.List_de_Capitais.Items[7] := Capitais.ExTerritorial;
  end
  
Else
  Begin
    ShowMessage ('Preencha todos os campos');
end;
end;

procedure TCapitais_principal.Bt_removerClick(Sender: TObject);
begin
 If (List_de_capitais.ItemIndex = 1) Then
    Begin
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := ' ';
    end;

    If (List_de_Capitais.ItemIndex = 3) Then
    Begin
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := ' ';
    end;

    If (List_de_Capitais.ItemIndex = 5) Then
    Begin
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := ' ';
    end;

    If (List_de_Capitais.ItemIndex = 7) Then
    Begin
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := ' ';
    end;
end;

procedure TCapitais_principal.Bt_atClick(Sender: TObject);
begin
//Atualizar nome
  If (List_de_Capitais.ItemIndex = 1) and (Ed_nome.Text <> '') and (Ed_nome.Text <> 'Nome da Capital') Then
  Begin
      Capitais.nome := Ed_nome.Text;
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := Capitais.nome;
  end


  //Atualizar pa�s
  Else If (List_de_Capitais.ItemIndex = 3) and (Ed_pais.Text <> '') and (Ed_pais.Text <> 'Pa�s') Then
  Begin
      Capitais.pais := Ed_pais.Text;
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := Capitais.pais;
  end


  //Atualizar numero de habitantes (nunca se sabe)
  Else If (List_de_Capitais.ItemIndex = 5) and (Ed_numerodehabitantes.Text <> '') and (Ed_numerodehabitantes.Text <> 'Numero de habitantes') Then
  Begin
      Capitais.numerodehabitantes := Ed_numerodehabitantes.Text;
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := Capitais.nome;
  end

  //Atualizar Extens�o
  Else If (List_de_Capitais.ItemIndex = 7) and (Ed_ExTerritorial.Text <> '') and (Ed_ExTerritorial.Text <> 'Tempo de vida') Then
  Begin
      Capitais.ExTerritorial := Ed_ExTerritorial.Text;
      Capitais_principal.List_de_Capitais.Items[List_de_Capitais.ItemIndex] := Capitais.ExTerritorial;
  end
  Else
    ShowMessage ('Selecione algum item');
end;

procedure TCapitais_principal.Bt_saveClick(Sender: TObject);
begin
AssignFile(CapitalI, Ed_nome.text + '.txt' );
  Rewrite(CapitalI);
  WriteLn(CapitalI, Capitais.nome) ;
  WriteLn(CapitalI, Capitais.pais);
  WriteLn(CapitalI, Capitais.numerodehabitantes);
  WriteLn(CapitalI, Capitais.ExTerritorial);
  CloseFile(CapitalI);

  ShowMessage('Arquivo salvo!');
end;

end.
